package com.orlandoxavier.ams.budget;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
public class AmsBudgetApplication {

    public static void main(String[] args) {
        SpringApplication.run(AmsBudgetApplication.class, args);
    }

}
