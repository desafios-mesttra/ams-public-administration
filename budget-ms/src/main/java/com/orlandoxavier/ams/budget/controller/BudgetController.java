package com.orlandoxavier.ams.budget.controller;

import com.orlandoxavier.ams.budget.model.Budget;
import com.orlandoxavier.ams.budget.model.dto.SpentDto;
import com.orlandoxavier.ams.budget.service.BudgetService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/budget")
@RequiredArgsConstructor
public class BudgetController {

    final BudgetService budgetService;

    @ApiOperation("Returns a list with all budgets with pagination option.")
    @GetMapping
    public List<Budget> all(@RequestParam(value = "pageSize", required = false) Integer pageSize,
                            @RequestParam(value = "pageIndex", required = false) Integer pageIndex) {
        return budgetService.all(pageSize, pageIndex);
    }

    @ApiOperation("Returns a list with all budgets by possible destination.")
    @GetMapping("/destination/{destinationId}")
    public List<Budget> getByPossibleDestination(@PathVariable Long destinationId) {
        return budgetService.getByPossibleDestination(destinationId);
    }

    @ApiOperation("Returns a budget by id.")
    @GetMapping("/{id}")
    public Budget get(@PathVariable Long id) {
        return budgetService.get(id);
    }

    @ApiOperation("Saves a budget and returns it.")
    @PostMapping
    public Budget save(@Valid @RequestBody Budget budget) {
        return budgetService.save(budget);
    }

    @ApiOperation("Updates the spentAmount of budget of received id.")
    @PatchMapping("/{id}/expense")
    public ResponseEntity<String> expense(@PathVariable Long id, @RequestBody SpentDto spentDto) {
        return budgetService.expense(id, spentDto);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();

        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
