package com.orlandoxavier.ams.budget.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "budgets")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Budget {

    @ApiModelProperty(value = "Budget id")
    @Id
    @GeneratedValue
    private long id;

    @ApiModelProperty(value = "Total amount")
    @NotNull
    @Min(value = 100000, message = "Total amount must be greater than 100000")
    @Column(name = "total_amount", nullable = false)
    private BigDecimal totalAmount;

    @ApiModelProperty(value = "Spent amount")
    @NotNull
    @Min(value = 1000, message = "Spent amount must be greater than 1000")
    @Column(name = "spent_amount", nullable = false)
    private BigDecimal spentAmount;

    @ApiModelProperty(value = "List of possible destinations - <Destination> class list")
    @ManyToMany(cascade = CascadeType.MERGE)
    @Valid
    @JoinTable(
            name = "budget_possible_destinations",
            joinColumns = @JoinColumn(name = "id_budget"),
            inverseJoinColumns = @JoinColumn(name = "id_possible_destination")
    )
    private List<Destination> possibleDestinations;

    @ApiModelProperty(value = "Origin of budget - <Origin> enum [FEDERAL|STATE|COUNTY]")
    @NotNull
    @Column(nullable = false)
    private Origin origin;

    @ApiModelProperty(value = "When was created")
    @CreationTimestamp
    @Column(nullable = false, updatable = false)
    private LocalDateTime createdAt;

    @ApiModelProperty(value = "When was updated")
    @UpdateTimestamp
    private LocalDateTime updatedAt;
}
