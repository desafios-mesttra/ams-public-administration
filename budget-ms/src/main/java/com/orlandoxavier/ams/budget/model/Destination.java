package com.orlandoxavier.ams.budget.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "destinations")
@Data
public class Destination {

    @ApiModelProperty(value = "Destination id")
    @Id
    private Long id;

    @ApiModelProperty(value = "Governmental folder")
    @Column(name = "folder", nullable = false, unique = true)
    private Folder folder;
}

