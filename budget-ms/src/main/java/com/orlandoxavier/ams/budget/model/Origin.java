package com.orlandoxavier.ams.budget.model;

public enum Origin {

    FEDERAL("Federal"),
    STATE("Estadual"),
    COUNTY("Municipal");

    private String value;

    Origin(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
