package com.orlandoxavier.ams.budget.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class SpentDto {

    @ApiModelProperty(value = "Spent amount of budget")
    private BigDecimal value;
}
