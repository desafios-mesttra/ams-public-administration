package com.orlandoxavier.ams.budget.repository;

import com.orlandoxavier.ams.budget.model.Budget;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BudgetRepository extends JpaRepository<Budget, Long> {
    @Query(nativeQuery = true,
            value = "SELECT b.* FROM budgets b, budget_possible_destinations bpd" +
                    " WHERE b.id = bpd.id_budget AND bpd.id_possible_destination = :destinationId")
    List<Budget> findByPossibleDestinationId(Long destinationId);
}
