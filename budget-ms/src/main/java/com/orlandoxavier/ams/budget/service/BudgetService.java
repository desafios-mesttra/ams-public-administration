package com.orlandoxavier.ams.budget.service;

import com.orlandoxavier.ams.budget.model.Budget;
import com.orlandoxavier.ams.budget.model.dto.SpentDto;
import com.orlandoxavier.ams.budget.repository.BudgetRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class BudgetService {

    final BudgetRepository budgetRepository;

    public List<Budget> all(Integer pageSize,
                            Integer pageIndex) {
        if (pageSize != null && pageIndex != null) {
            return budgetRepository.findAll(PageRequest.of(pageIndex, pageSize, Sort.by("totalAmount"))).getContent();
        }
        return budgetRepository.findAll();
    }

    public List<Budget> getByPossibleDestination(Long destinationId) {
        return budgetRepository.findByPossibleDestinationId(destinationId);
    }

    public Budget get(Long id) {
        return budgetRepository.findById(id).orElse(null);
    }

    public Budget save(Budget budget) {
        Optional<Budget> saved = Optional.of(budgetRepository.save(budget));
        return saved.orElse(null);
    }

    public ResponseEntity<String> expense(Long id, SpentDto spentDto) {
        Optional<Budget> budget = budgetRepository.findById(id);

        if (budget.isPresent()) {
            if (spentDto.getValue().compareTo(budget.get().getTotalAmount().subtract(budget.get().getSpentAmount())) < 0) {
                budget.get().setSpentAmount(spentDto.getValue());
                budgetRepository.save(budget.get());
                return ResponseEntity.status(HttpStatus.OK).body("Computed. :)");
            }
            return ResponseEntity.status(HttpStatus.OK).body("Not changed. :)");
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No budget could be found. :(");
    }
}
