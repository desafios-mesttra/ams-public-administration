package com.orlandoxavier.ams.budget.controller;

import static org.mockito.Mockito.*;

import com.orlandoxavier.ams.budget.model.Budget;
import com.orlandoxavier.ams.budget.model.Destination;
import com.orlandoxavier.ams.budget.model.Folder;
import com.orlandoxavier.ams.budget.model.Origin;
import com.orlandoxavier.ams.budget.repository.BudgetRepository;
import com.orlandoxavier.ams.budget.service.BudgetService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class BudgetControllerTest {

    private final BudgetService mockedBudgeService = mock(BudgetService.class);
    private final BudgetRepository mockedBudgetRepository = mock(BudgetRepository.class);
    private BudgetController budgetController = new BudgetController(mockedBudgeService);

    @Test
    public void whenPageSizeAndPageIndexWasNullShouldCallFindAllWithoutParameters() {
        budgetController.all(null, null);
        verify(mockedBudgeService).all(null, null);
    }

    @Test
    public void whenSaveBudgetShouldReturnBudget() {
        Budget budget = createBudget();
        when(mockedBudgetRepository.save(ArgumentMatchers.any(Budget.class))).thenReturn(budget);
        Budget created = mockedBudgetRepository.save(budget);
        Assertions.assertEquals(created.getTotalAmount(), budget.getTotalAmount());
        verify(mockedBudgetRepository).save(budget);
    }

    private Budget createBudget() {
        Budget budget = new Budget();
        budget.setTotalAmount(new BigDecimal(1000000));
        budget.setSpentAmount(new BigDecimal(50000));
        budget.setOrigin(Origin.COUNTY);
        budget.setPossibleDestinations(createDestinationList());

        return budget;
    }

    private List<Destination> createDestinationList() {
        List<Destination> destinationList = new ArrayList<>();
        Destination destination = new Destination();
        destination.setFolder(Folder.CULTURE);
        destinationList.add(destination);

        return destinationList;
    }
}
