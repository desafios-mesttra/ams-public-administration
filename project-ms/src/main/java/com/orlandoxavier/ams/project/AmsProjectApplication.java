package com.orlandoxavier.ams.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class AmsProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(AmsProjectApplication.class, args);
	}

}
