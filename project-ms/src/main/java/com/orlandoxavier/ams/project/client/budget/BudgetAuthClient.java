package com.orlandoxavier.ams.project.client.budget;

import com.orlandoxavier.ams.project.model.auth.AuthRequest;
import com.orlandoxavier.ams.project.model.auth.AuthResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "budget-auth", url = "http://localhost:8080/auth")
public interface BudgetAuthClient {

    @PostMapping
    AuthResponse auth(@RequestBody AuthRequest authRequest);

}
