package com.orlandoxavier.ams.project.client.budget;

import com.orlandoxavier.ams.project.client.budget.dto.Budget;
import com.orlandoxavier.ams.project.config.FeignBudgetConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "budget-ms", configuration = FeignBudgetConfig.class, url = "http://localhost:8080")
public interface BudgetClient {

    @GetMapping(value = "/budget/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    Budget get(@PathVariable("id") Long id);
}
