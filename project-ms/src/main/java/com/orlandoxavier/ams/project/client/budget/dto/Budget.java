package com.orlandoxavier.ams.project.client.budget.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonDeserialize
public class Budget implements Serializable {

    private long id;
    private BigDecimal totalAmount;
    private BigDecimal spentAmount;
    private List<Destination> possibleDestinations;
    private Origin origin;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
}
