package com.orlandoxavier.ams.project.client.budget.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonDeserialize
public class Destination implements Serializable {

    private Long id;
    private Folder folder;
}

