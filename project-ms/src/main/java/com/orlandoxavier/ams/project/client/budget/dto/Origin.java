package com.orlandoxavier.ams.project.client.budget.dto;

public enum Origin {

    FEDERAL("Federal"),
    STATE("Estadual"),
    COUNTY("Municipal");

    private String value;

    Origin(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
