package com.orlandoxavier.ams.project.client.secretary;

import com.orlandoxavier.ams.project.model.auth.AuthRequest;
import com.orlandoxavier.ams.project.model.auth.AuthResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "secretary-auth", url = "http://localhost:8180/auth")
public interface SecretaryAuthClient {

    @PostMapping
    AuthResponse auth(@RequestBody AuthRequest authRequest);

}
