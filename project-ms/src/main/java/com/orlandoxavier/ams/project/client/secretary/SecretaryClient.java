package com.orlandoxavier.ams.project.client.secretary;

import com.orlandoxavier.ams.project.config.FeignSecretaryConfig;
import com.orlandoxavier.ams.project.client.secretary.dto.Secretary;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "secretary-ms", configuration = FeignSecretaryConfig.class, url = "http://localhost:8180")
public interface SecretaryClient {

    @GetMapping(value = "/secretary/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    Secretary get(@PathVariable("id") Long id);

}
