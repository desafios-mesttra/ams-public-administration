package com.orlandoxavier.ams.project.client.secretary.dto;

import java.io.Serializable;

public enum Folder implements Serializable {
    HEALTH("Saúde"),
    EDUCATION("Educação"),
    SPORTS("Esportes"),
    INFRASTRUCTURE("Infraestrutura"),
    CULTURE("Cultura"),
    LABOUR("Trabalho");

    private String value;

    Folder(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
