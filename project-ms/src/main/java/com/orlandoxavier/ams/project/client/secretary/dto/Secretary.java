package com.orlandoxavier.ams.project.client.secretary.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonDeserialize
public class Secretary implements Serializable {

    private Long id;
    private Folder folder;
    private String bossName;
    private Float populationGrade;
    private boolean underInvestigation;
}