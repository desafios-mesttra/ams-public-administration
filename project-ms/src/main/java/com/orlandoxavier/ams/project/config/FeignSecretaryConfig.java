package com.orlandoxavier.ams.project.config;

import com.orlandoxavier.ams.project.model.auth.TokenEntity;
import com.orlandoxavier.ams.project.repository.TokenRepository;
import com.orlandoxavier.ams.project.service.TokenService;
import com.orlandoxavier.ams.project.service.UserDetailService;
import com.orlandoxavier.ams.project.util.JwtUtil;
import feign.*;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@RequiredArgsConstructor
public class FeignSecretaryConfig implements RequestInterceptor {

    final TokenRepository tokenRepository;
    final JwtUtil jwtUtil;
    final UserDetailService userDetailService;
    final TokenService tokenService;

    @Override
    public void apply(RequestTemplate requestTemplate) {
        if (Interceptor.targetInterceptor.equals(Interceptor.TargetInterceptor.SECRETARY)) {
            requestTemplate.header("Authorization", "Bearer " + getValidToken().getHash());
            System.out.println(requestTemplate.toString());
        }
    }

    private TokenEntity getValidToken() {
        List<TokenEntity> list = tokenRepository.findAllByOrigin(TokenEntity.Origin.SECRETARY);
        if (list != null && !list.isEmpty()) {
            TokenEntity token = list.get(0);
            if (token != null) {
                if (jwtUtil.validateToken(token.getHash(), userDetailService.loadUserByUsername("foo"))) {
                    return token;
                }
            }
        }
        return refreshToken();
    }

    private TokenEntity refreshToken() {
        List<TokenEntity> list = tokenRepository.findAllByOrigin(TokenEntity.Origin.SECRETARY);
        if (list != null && !list.isEmpty()) {
            TokenEntity oldToken = list.get(0);
            if (oldToken != null) {
                tokenRepository.delete(oldToken);
            }
        }

        TokenEntity token = tokenService.authenticateInSecretary();
        if (token != null) {
            return tokenService.saveToken(token);
        }
        throw new RuntimeException("Cannot refresh secretary-ms token.");
    }
}
