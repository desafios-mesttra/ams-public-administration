package com.orlandoxavier.ams.project.config;

public class Interceptor {

    public enum TargetInterceptor {
        BUDGET,
        SECRETARY,
        UNDEFINED
    }

    public static TargetInterceptor targetInterceptor = TargetInterceptor.UNDEFINED;

    public static void budget() {
        targetInterceptor = TargetInterceptor.BUDGET;
    }

    public static void secretary() {
        targetInterceptor = TargetInterceptor.SECRETARY;
    }
}
