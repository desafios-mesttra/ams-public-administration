package com.orlandoxavier.ams.project.controller;

import com.orlandoxavier.ams.project.client.budget.BudgetClient;
import com.orlandoxavier.ams.project.client.budget.dto.Budget;
import com.orlandoxavier.ams.project.config.Interceptor;
import com.orlandoxavier.ams.project.model.Project;
import com.orlandoxavier.ams.project.service.ProjectService;
import com.orlandoxavier.ams.project.service.TokenService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/project")
@RequiredArgsConstructor
public class ProjectController {

    final ProjectService projectService;
    final TokenService tokenService;
    final BudgetClient budgetClient;

    @ApiOperation("Returns a list with all projects with pagination option.")
    @GetMapping
    public List<Project> all(@RequestParam(required = false) Integer pageSize,
                             @RequestParam(required = false) Integer pageIndex) {
        return projectService.all(pageSize, pageIndex);
    }

    @ApiOperation("Returns a project by id.")
    @GetMapping("/{id}")
    public Project get(@PathVariable Long id) {
        return projectService.get(id);
    }

    @ApiOperation("Saves a project and returns it.")
    @PostMapping
    public ResponseEntity<?> save(@Valid @RequestBody Project project) {
        return projectService.createFullProject(project);
    }

    @GetMapping("/getBudget/{budgetId}")
    public Budget getBudget(@PathVariable Long budgetId) {
        Interceptor.budget();
        return budgetClient.get(budgetId);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();

        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
