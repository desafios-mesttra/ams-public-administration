package com.orlandoxavier.ams.project.model;


public enum Folder {

    HEALTH("Saúde"),
    EDUCATION("Educação"),
    SPORTS("Esportes"),
    INFRASTRUCTURE("Infraestrutura"),
    CULTURE("Cultura"),
    LABOUR("Trabalho");

    private String value;

    Folder(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
