package com.orlandoxavier.ams.project.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;

@Entity
@Table(name = "projects")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Project {

    @ApiModelProperty("Project id")
    @Id
    @GeneratedValue
    private Long id;

    @ApiModelProperty("Secretary id")
    @NotNull(message = "Secretary id is mandatory")
    @Column(name = "id_secretary", nullable = false)
    private Long secretaryId;

    @ApiModelProperty("Budget id")
    @NotNull(message = "Budget id is mandatory")
    @Column(name = "id_budget", nullable = false)
    private Long budgetId;

    @ApiModelProperty("Project cost")
    @NotNull
    @Min(value = 1000, message = "Cost must be greater than 1000")
    @Column(nullable = false)
    private BigDecimal cost;

    @ApiModelProperty("Project title")
    @NotNull
    @NotBlank(message = "Title is mandatory")
    @Column(nullable = false)
    private String title;

    @ApiModelProperty("Project description")
    @NotNull
    @NotBlank(message = "Description is mandatory")
    @Column(nullable = false, length = 1000)
    private String description;

    @ApiModelProperty(value = "Governmental folder")
    @NotNull(message = "Folder is mandatory")
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Folder folder;
}
