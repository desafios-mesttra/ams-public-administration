package com.orlandoxavier.ams.project.model.auth;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import java.io.Serializable;

@RedisHash("token")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TokenEntity implements Serializable {

    public enum Origin {
        BUDGET,
        SECRETARY
    }

    @Id
    private String id;

    @Column(nullable = false)
    private String hash;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Origin origin;
}
