package com.orlandoxavier.ams.project.repository;

import com.orlandoxavier.ams.project.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<Project, Long> {
}
