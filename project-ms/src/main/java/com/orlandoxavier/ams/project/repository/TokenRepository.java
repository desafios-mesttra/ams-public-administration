package com.orlandoxavier.ams.project.repository;

import com.orlandoxavier.ams.project.model.auth.TokenEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TokenRepository extends CrudRepository<TokenEntity, String> {

    List<TokenEntity> findAllByOrigin(TokenEntity.Origin origin);
}
