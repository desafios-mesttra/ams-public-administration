package com.orlandoxavier.ams.project.service;

import com.orlandoxavier.ams.project.client.budget.BudgetClient;
import com.orlandoxavier.ams.project.client.secretary.SecretaryClient;
import com.orlandoxavier.ams.project.config.Interceptor;
import com.orlandoxavier.ams.project.model.Project;
import com.orlandoxavier.ams.project.repository.ProjectRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProjectService {

    final ProjectRepository projectRepository;
    final SecretaryClient secretaryClient;
    final BudgetClient budgetClient;

    public List<Project> all(Integer pageSize, Integer pageIndex) {
        if (pageIndex != null && pageSize != null) {
            return projectRepository.findAll(PageRequest.of(pageIndex, pageSize, Sort.by("cost"))).getContent();
        }
        return projectRepository.findAll();
    }

    public Project get(Long id) {
        return projectRepository.findById(id).orElse(null);
    }

    public Project save(Project project) {
        return projectRepository.save(project);
    }

    public ResponseEntity<?> createFullProject(Project project) {
        Interceptor.secretary();
        var secretaryDto = secretaryClient.get(project.getSecretaryId());
        if (secretaryDto == null) {
            return ResponseEntity.badRequest().body("Secretary not found.");
        }
        if (secretaryDto.isUnderInvestigation()) {
            return ResponseEntity.badRequest().body("The secretary cannot be attached to project because it is under investigation.");
        }
        if (!secretaryDto.getFolder().getValue().equals(project.getFolder().getValue())) {
            return ResponseEntity.badRequest().body("The project folder should be the same of the secretary folder.");
        }

        Interceptor.budget();
        var budgetDto = budgetClient.get(project.getBudgetId());
        if (budgetDto == null) {
            return ResponseEntity.badRequest().body("Budget not found.");
        }
        if ((budgetDto.getTotalAmount().subtract(budgetDto.getSpentAmount())).compareTo(project.getCost()) < 0) {
            return ResponseEntity.badRequest().body("The cost of project cannot be greater than the available in the budget.");
        }

        project = save(project);
        return ResponseEntity.ok().body(project);
    }
}
