package com.orlandoxavier.ams.project.service;

import com.orlandoxavier.ams.project.client.budget.BudgetAuthClient;
import com.orlandoxavier.ams.project.client.secretary.SecretaryAuthClient;
import com.orlandoxavier.ams.project.model.auth.AuthRequest;
import com.orlandoxavier.ams.project.model.auth.TokenEntity;
import com.orlandoxavier.ams.project.repository.TokenRepository;
import com.orlandoxavier.ams.project.util.JwtUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class TokenService {

    final TokenRepository tokenRepository;
    final SecretaryAuthClient secretaryAuthClient;
    final BudgetAuthClient budgetAuthClient;
    final JwtUtil jwtUtil;
    final UserDetailService userDetailService;

    private AuthRequest getDefaultAuthRequest() {
        return new AuthRequest("foo", "foo");
    }

    public TokenEntity getToken(TokenEntity.Origin origin) {
        List<TokenEntity> list = tokenRepository.findAllByOrigin(origin);
        return (list != null && !list.isEmpty()) ? list.get(0) : null;
    }

    public TokenEntity save(TokenEntity token) {
        return tokenRepository.save(token);
    }

    public void delete(TokenEntity token) {
        tokenRepository.delete(token);
    }

    public TokenEntity authenticateInSecretary() {
        var jwt = secretaryAuthClient.auth(getDefaultAuthRequest());
        if (jwt == null) {
            throw new RuntimeException("Can't auth in secretary-ms.");
        }
        TokenEntity token = new TokenEntity(UUID.randomUUID().toString(), jwt.getJwt(), TokenEntity.Origin.SECRETARY);
        return token;
    }

    public TokenEntity authenticateInBudget() {
        var jwt = budgetAuthClient.auth(getDefaultAuthRequest());
        if (jwt == null) {
            throw new RuntimeException("Can't auth in budget-ms.");
        }
        TokenEntity token = new TokenEntity(UUID.randomUUID().toString(), jwt.getJwt(), TokenEntity.Origin.BUDGET);
        return token;
    }

    public TokenEntity saveToken(TokenEntity token) {
        return tokenRepository.save(token);
    }
}
