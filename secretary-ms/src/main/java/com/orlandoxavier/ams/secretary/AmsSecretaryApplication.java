package com.orlandoxavier.ams.secretary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AmsSecretaryApplication {

	public static void main(String[] args) {
		SpringApplication.run(AmsSecretaryApplication.class, args);
	}

}
