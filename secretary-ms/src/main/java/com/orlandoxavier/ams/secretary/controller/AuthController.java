package com.orlandoxavier.ams.secretary.controller;

import com.orlandoxavier.ams.secretary.model.auth.AuthRequest;
import com.orlandoxavier.ams.secretary.model.auth.AuthResponse;
import com.orlandoxavier.ams.secretary.util.JwtUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AuthController {

    final AuthenticationManager authenticationManager;
    final UserDetailsService userDetailsService;
    final JwtUtil jwtUtil;

    @PostMapping("/auth")
    public ResponseEntity<?> createAuthToken(@RequestBody AuthRequest authRequest) throws Exception {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword())
            );
        } catch (BadCredentialsException ex) {
            throw new Exception("Invalid credentials. :(");
        }

        final UserDetails userDetails = userDetailsService.loadUserByUsername(authRequest.getUsername());
        final String jwt = jwtUtil.generateToken(userDetails);

        System.out.println("* [JWT] ########## 1 generated ##########");
        return ResponseEntity.ok(new AuthResponse(jwt));
    }
}
