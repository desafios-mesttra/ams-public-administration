package com.orlandoxavier.ams.secretary.controller;

import com.orlandoxavier.ams.secretary.model.Secretary;
import com.orlandoxavier.ams.secretary.model.dto.InvestigationDto;
import com.orlandoxavier.ams.secretary.service.SecretaryService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/secretary")
@RequiredArgsConstructor
public class SecretaryController {

    final SecretaryService secretaryService;

    @ApiOperation("Returns a list with all secretaries with pagination option.")
    @GetMapping
    public List<Secretary> all(@RequestParam(value = "pageSize", required = false) Integer pageSize,
                               @RequestParam(value = "pageIndex", required = false) Integer pageIndex) {
        return secretaryService.all(pageSize, pageIndex);
    }

    @ApiOperation("Returns a secretary by id.")
    @GetMapping("/{id}")
    public Secretary get(@PathVariable Long id) {
        return secretaryService.get(id);
    }

    @ApiOperation("Saves a secretary and returns it or and bad request if there is already a secretary in this folder.")
    @PostMapping
    public ResponseEntity<?> save(@Valid @RequestBody Secretary secretary) {
        if (secretaryService.existsInFolder(secretary.getFolder())) {
            return new ResponseEntity<>("There is already a secretary in this folder..", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(secretaryService.save(secretary), HttpStatus.CREATED);
    }

    @ApiOperation("Updates the underInvestigation attribute of secretary with received id.")
    @PatchMapping("/{id}/investigation")
    public ResponseEntity<String> investigation(@PathVariable Long id, @RequestBody InvestigationDto investigationDto) {
        return secretaryService.investigation(id, investigationDto);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();

        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
