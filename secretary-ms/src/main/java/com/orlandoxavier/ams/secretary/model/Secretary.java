package com.orlandoxavier.ams.secretary.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "secretaries")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Secretary {

    @ApiModelProperty(value = "Secretary id")
    @Id
    @GeneratedValue
    private Long id;

    @ApiModelProperty(value = "Governmental folder")
    @NotNull(message = "Folder is mandatory")
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, unique = true)
    private Folder folder;

    @ApiModelProperty(value = "Secretary boss name")
    @NotBlank(message = "Boss name is mandatory")
    @Column(name = "boss_name", nullable = false)
    private String bossName;

    @ApiModelProperty(value = "Total population")
    @NotNull(message = "Boss name is mandatory")
    @Min(value = 10, message = "Population grade must be greater than 10")
    @Column(name = "population_grade", nullable = false)
    private Float populationGrade;

    @ApiModelProperty(value = "About it's under investigation")
    @Column(name = "under_investigation", nullable = false, columnDefinition = "boolean default false")
    private boolean underInvestigation;
}

