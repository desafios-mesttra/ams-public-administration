package com.orlandoxavier.ams.secretary.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class InvestigationDto {

    @ApiModelProperty(value = "About it's under investigation")
    private boolean underInvestigation;
}
