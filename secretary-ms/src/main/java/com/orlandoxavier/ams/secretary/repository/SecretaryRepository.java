package com.orlandoxavier.ams.secretary.repository;

import com.orlandoxavier.ams.secretary.model.Secretary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface SecretaryRepository extends JpaRepository<Secretary, Long> {

    @Query(nativeQuery = true, value = "SELECT s.* FROM secretaries s WHERE folder = :folder LIMIT 1")
    Secretary getByFolder(String folder);
}
