package com.orlandoxavier.ams.secretary.service;

import com.orlandoxavier.ams.secretary.model.Folder;
import com.orlandoxavier.ams.secretary.model.dto.InvestigationDto;
import com.orlandoxavier.ams.secretary.model.Secretary;
import com.orlandoxavier.ams.secretary.repository.SecretaryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SecretaryService {

    final SecretaryRepository secretaryRepository;

    public List<Secretary> all(Integer pageSize,
                               Integer pageIndex) {
        if (pageSize != null && pageIndex != null) {
            return secretaryRepository.findAll(PageRequest.of(pageIndex, pageSize, Sort.by("folder"))).getContent();
        }
        return secretaryRepository.findAll();
    }

    public Secretary get(Long id) {
        return secretaryRepository.findById(id).orElse(null);
    }

    public boolean existsInFolder(Folder folder) {
        return secretaryRepository.getByFolder(folder.toString()) != null;
    }

    public Secretary save(Secretary secretary) {
        if (existsInFolder(secretary.getFolder())) {
            return null;
        }
        Optional<Secretary> saved = Optional.of(secretaryRepository.save(secretary));
        return saved.orElse(null);
    }

    public ResponseEntity<String> investigation(Long id, InvestigationDto investigationDto) {
        Optional<Secretary> secretary = secretaryRepository.findById(id);

        if (secretary.isPresent()) {
            secretary.get().setUnderInvestigation(investigationDto.isUnderInvestigation());
            secretaryRepository.save(secretary.get());
            return ResponseEntity.status(HttpStatus.OK).body("Changed. :)");
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No secretary could be found with this id. :(");
    }
}
