package com.orlandoxavier.ams.secretary.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class JsonUtil {

    public String convertToJson(Object object) throws JsonProcessingException {
        return object != null ? new ObjectMapper().writeValueAsString(object) : null;
    }
}
